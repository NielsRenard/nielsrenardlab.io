---
title: About
---

 .. role:: strike

Hi, I'm Niels. I'm employed as a software engineer, based in the Netherlands. I write code in a functional style and favor programming languages that `push <https://www.haskell.org/>`_ `you <https://www.rust-lang.org/>`_ `in <https://elm-lang.org/>`_ `that <https://clojure.org/>`_ direction. Most of my spare time goes into trying to do game development and studying `Japanese <https://nielsrenard.gitlab.io/nihongo/>`_.

**Current reading**

- `Computer Systems: A Programmer's Perspective <http://csapp.cs.cmu.edu/3e/home.html>`_
  | `my progress and exercises <https://nielsrenard.gitlab.io/book-computer-systems-a-programmers-perspective/>`_

- `Roguelike Tutorial <http://bfnightly.bracketproductions.com/rustbook>`_
  | :strike:`section 1, section 2,` `section 3`
  | `my progress <https://gitlab.com/NielsRenard/rust-roguelike-tutorial#section-2-stretch-goals>`_

**Past reading**

- `The Rust Programming Language <https://nostarch.com/Rust2018>`_
  | `my exercises & notes <https://gitlab.com/NielsRenard/book-the-rust-programming-language#progress>`_
