---
title: "\"Iced\": The Elm architecture, but in Rust"
description: "I put together a japanese dictionary and Anki flash card generator app"
author: Niels
---

[https://gitlab.com/NielsRenard/dict](https://gitlab.com/NielsRenard/dict)

<img src = "https://gitlab.com/NielsRenard/dict/-/raw/master/img/generate_flashcards.gif" width="720">

## Introduction
Last month I wanted to try out [iced](https://github.com/iced-rs/iced): a Rust framework for building cross platform desktop (and web) applications. I also needed a quick way to create japanese flash cards for use in [anki](https://apps.ankiweb.net/) (a free and open-source flashcard program using spaced repetition). This all seemed to fit so I put together a small dictionary application using the [jisho](https://jisho.org/) search api (found [here](https://jisho.org/api/v1/search/words?keyword=something)) and a huge set of japanese example sentences (found on [tatoeba.org](https://tatoeba.org/en/downloads)). 

## Iced
Iced uses [The Elm Architecture (TEA)](https://guide.elm-lang.org/architecture/) to structure your application. I have a sizable sideproject ([BeerBinder.com](https://beerbinder.com)) running in Elm so I'm familiar with TEA, which made picking up iced a breeze. Most of the work went into [writing the parser](https://gitlab.com/NielsRenard/dict/-/blob/master/src/example_sentences.rs) for the [example sentence format](http://www.edrdg.org/wiki/index.php/Tanaka_Corpus#Incorporation_into_the_WWWJDIC_Server).

## Parsing with nom
I used the very mature parser combinators library [nom](https://github.com/Geal/nom), and am quite happy with the way it turned out. I always think using parser combinators is a lot of fun. At startup it parses ~150K Japanese sentence strings into my ExampleSentence datastructure, which takes around 100ms on my 4 year old thinkpad. It then spends another 2 seconds indexing them using the default Rust HashMap.

## What's next

I'm using the application daily, because it's a lot faster than navigating multiple web pages, and can create flashcards with 1 click. Nonetheless there's a lot I want to improve still (words with multiple meanings, sorting by presence of example sentences, cross-platform installation, web version) and features I want to add (update example sentences, generate furigana, toggle translation visiblity, expanded flashcard generation, cloze sentences). It would also be nice to turn the parser into a crate, so other people can use it, and expand it with more sources.
