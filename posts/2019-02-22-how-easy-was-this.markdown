---
title: Simple static site setup
description: Using gitlab pages to set up a blog in 5 steps
author: Niels
---

1. Fork project from https://gitlab.com/pages
2. Change project-name to yourusername.gitlab.io in Settings
3. Hit "start pipeline"  
4. Add new Markdown files to posts.
5. That's it.
6. Forget about this blog and feel bad in two years when there are no posts.
